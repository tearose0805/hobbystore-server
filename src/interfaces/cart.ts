export interface ICartItem {
  itemID: string;
  quantity: number;
  category: string;
  subcategory: string;
}
export interface ICartItemUID extends ICartItem {
  UID: string;
}

export interface ICart {
  UID: string;
  cart: ICartItem[];
}
