export interface IProductStock {
  itemId: string;
  stock: number;
  reserve: number;
}
