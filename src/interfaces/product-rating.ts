export interface IProductRating {
  rating1: number;
  rating2: number;
  rating3: number;
  rating4: number;
  rating5: number;
}
