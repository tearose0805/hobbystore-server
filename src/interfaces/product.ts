export interface IProduct {
  title: string;
  type: string;
  price: number;
  description: string;
  img: string;
  img_thumb: string;
  category: string;
  subcategory: string;
  product_number: string;
  rating1: number;
  rating2: number;
  rating3: number;
  rating4: number;
  rating5: number;
  size?: string;
  weight?: string;
  pack?: string;
  color?: string;
  quantity?: string;
}
