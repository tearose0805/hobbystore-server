import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { IProductStock } from '../interfaces/product-stock';

export type PaperProductDocument = HydratedDocument<PaperProductStock>;

@Schema()
export class PaperProductStock implements IProductStock {
  @Prop() itemId: string;
  @Prop() stock: number;
  @Prop() reserve: number;
}
export const PaperProductStockSchema =
  SchemaFactory.createForClass(PaperProductStock);
