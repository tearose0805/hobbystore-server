import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { IOrder } from '../interfaces/order';
import { HydratedDocument } from 'mongoose';

export type OrderDocument = HydratedDocument<Order>;

@Schema()
export class Order implements IOrder {
  @Prop() firstName: string;
  @Prop() secondName?: string;
  @Prop() lastName: string;
  @Prop() mobile: string;
  @Prop() index?: string;
  @Prop() region: string;
  @Prop() city: string;
  @Prop() address: string;
  @Prop() orderNumber: number;
  @Prop() goodsItems: {
    itemID: string;
    title: string;
    img: string;
    product_number: string;
    quantity: number;
    category: string;
    subcategory: string;
    price: number;
    stock: number;
  }[];
  @Prop() orderInfo: {
    order: number;
    delivery_type: string;
    delivery_price: number;
    total: number;
    orderNumber: number;
  }[];
}

export const OrderSchema = SchemaFactory.createForClass(Order);
