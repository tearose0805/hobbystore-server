import { HydratedDocument } from 'mongoose';
import { IProductStock } from '../interfaces/product-stock';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ColoringStockDocument = HydratedDocument<ColoringStock>;

@Schema()
export class ColoringStock implements IProductStock {
  @Prop() itemId: string;
  @Prop() stock: number;
  @Prop() reserve: number;
}
export const ColoringStockSchema = SchemaFactory.createForClass(ColoringStock);
