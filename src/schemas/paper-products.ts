import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { IProduct } from '../interfaces/product';

export type PaperProductDocument = HydratedDocument<PaperProduct>;

@Schema()
export class PaperProduct implements IProduct {
  @Prop() title: string;
  @Prop() type: string;
  @Prop() price: number;
  @Prop() description: string;
  @Prop() category: string;
  @Prop() subcategory: string;
  @Prop() img: string;
  @Prop() img_thumb: string;
  @Prop() size?: string;
  @Prop() weight?: string;
  @Prop() pack?: string;
  @Prop() color?: string;
  @Prop() quantity?: string;
  @Prop() product_number: string;
  @Prop() rating1: number;
  @Prop() rating2: number;
  @Prop() rating3: number;
  @Prop() rating4: number;
  @Prop() rating5: number;
}

export const PaperProductSchema = SchemaFactory.createForClass(PaperProduct);
