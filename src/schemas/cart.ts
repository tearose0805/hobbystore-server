import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { ICart } from '../interfaces/cart';

export type CartDocument = HydratedDocument<Cart>;

@Schema()
export class Cart implements ICart {
  @Prop() UID: string;
  @Prop() cart: {
    itemID: string;
    quantity: number;
    category: string;
    subcategory: string;
  }[];
}

export const CartSchema = SchemaFactory.createForClass(Cart);
