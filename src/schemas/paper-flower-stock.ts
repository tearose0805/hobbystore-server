import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { IProductStock } from '../interfaces/product-stock';

export type PaperFlowerStockDocument = HydratedDocument<PaperFlowerStock>;

@Schema()
export class PaperFlowerStock implements IProductStock {
  @Prop() itemId: string;
  @Prop() stock: number;
  @Prop() reserve: number;
}
export const PaperFlowerStockSchema =
  SchemaFactory.createForClass(PaperFlowerStock);
