import { HydratedDocument } from 'mongoose';
import { IProductStock } from '../interfaces/product-stock';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type ArtSupplyStockDocument = HydratedDocument<ArtSupplyStock>;

@Schema()
export class ArtSupplyStock implements IProductStock {
  @Prop() itemId: string;
  @Prop() stock: number;
  @Prop() reserve: number;
}
export const ArtSupplyStockSchema =
  SchemaFactory.createForClass(ArtSupplyStock);
