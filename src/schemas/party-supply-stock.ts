import { HydratedDocument } from 'mongoose';
import { IProductStock } from '../interfaces/product-stock';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type PartSupplyStockDocument = HydratedDocument<PartySupplyStock>;

@Schema()
export class PartySupplyStock implements IProductStock {
  @Prop() itemId: string;
  @Prop() stock: number;
  @Prop() reserve: number;
}
export const PartySupplyStockSchema =
  SchemaFactory.createForClass(PartySupplyStock);
