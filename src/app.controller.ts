import { Controller, Delete, Get, Post, Put } from '@nestjs/common';

@Controller()
export class AppController {
  // eslint-disable-next-line @typescript-eslint/no-empty-function

  @Get()
  getHello(): string {
    return ' Hi';
  }

  @Post()
  sendAll(): string {
    return 'post data';
  }

  @Put()
  update(): string {
    return 'put data';
  }

  @Delete()
  delete(): string {
    return 'delete data';
  }
}
