import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GoodsByCategoryModule } from './controllers/goods-by-category/goods-by-category.module';
import { CartModule } from './controllers/cart/cart.module';
import { OrderModule } from './controllers/order/order.module';

@Module({
  imports: [
    GoodsByCategoryModule,
    CartModule,
    OrderModule,
    MongooseModule.forRoot('mongodb://127.0.0.1:27017/hobbystore'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
