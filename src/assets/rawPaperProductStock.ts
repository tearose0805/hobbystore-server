export const PaperProductStockArr = [
  /*0*/ {
    id: '64a270f75f99834d221a5fd3',
    stock: 3,
    reserve: 0,
  },

  /*1*/ {
    id: '64a270f75f99834d221a5fd5',
    stock: 1,
    reserve: 0,
  },

  /*2*/ {
    id: '64a270f75f99834d221a5fd7',
    stock: 10,
    reserve: 0,
  },

  /*3*/ {
    id: '64a270f75f99834d221a5fd9',
    stock: 11,
    reserve: 0,
  },

  /*4*/ {
    id: '64a270f75f99834d221a5fdb',
    stock: 11,
    reserve: 0,
  },

  /*5*/ {
    id: '64a270f75f99834d221a5fdd',
    stock: 12,
    reserve: 0,
  },

  /*6*/ {
    id: '64a270f75f99834d221a5fdf',
    stock: 15,
    reserve: 0,
  },

  /*7*/ {
    id: '64a270f75f99834d221a5fe1',
    stock: 17,
    reserve: 0,
  },

  /*8*/ {
    id: '64a270f75f99834d221a5fe3',
    stock: 10,
    reserve: 0,
  },

  /*9*/ {
    id: '64a270f75f99834d221a5fe5',
    stock: 11,
    reserve: 0,
  },

  /*10*/ {
    id: '64a270f75f99834d221a5fe7',
    stock: 9,
    reserve: 0,
  },

  /*11*/ {
    id: '64a270f75f99834d221a5fe9',
    stock: 13,
    reserve: 0,
  },

  /*12*/ {
    id: '64a270f75f99834d221a5feb',
    stock: 14,
    reserve: 0,
  },

  /*13*/ {
    id: '64a270f75f99834d221a5fed',
    stock: 8,
    reserve: 0,
  },

  /*14*/ {
    id: '64a270f75f99834d221a5fef',
    stock: 15,
    reserve: 0,
  },

  /*15*/ {
    id: '64a270f75f99834d221a5ff1',
    stock: 16,
    reserve: 0,
  },

  /*16*/ {
    id: '64a270f75f99834d221a5ff3',
    stock: 10,
    reserve: 0,
  },
];
