export const ColoringStockArr = [
  /*0*/ {
    id: '64b275c300311725635fc8fc',
    stock: 24,
    reserve: 0,
  },

  /*1*/ {
    id: '64b275c300311725635fc8fe',
    stock: 20,
    reserve: 0,
  },

  /*2*/ {
    id: '64b275c300311725635fc900e',
    stock: 15,
    reserve: 0,
  },

  /*3*/ {
    id: '64b275c300311725635fc902',
    stock: 10,
    reserve: 0,
  },

  /*4*/ {
    id: '64b275c300311725635fc904',
    stock: 12,
    reserve: 0,
  },

  /*5*/ {
    id: '64b275c300311725635fc906',
    stock: 14,
    reserve: 0,
  },

  /*6*/ {
    id: '64b275c300311725635fc908',
    stock: 13,
    reserve: 0,
  },

  /*7*/ {
    id: '64b275c300311725635fc90a',
    stock: 9,
    reserve: 0,
  },

  /*8*/ {
    id: '64b275c300311725635fc90c',
    stock: 8,
    reserve: 0,
  },
];
