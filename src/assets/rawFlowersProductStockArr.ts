export const PaperFlowerStockArr = [
  /*0*/ {
    id: '64b2516ba3999fa0cc45f8c3',
    stock: 7,
    reserve: 0,
  },

  /*1*/ {
    id: '64b2516ba3999fa0cc45f8c5',
    stock: 10,
    reserve: 0,
  },

  /*2*/ {
    id: '64b2516ba3999fa0cc45f8c7',
    stock: 12,
    reserve: 0,
  },

  /*3*/ {
    id: '64b2516ba3999fa0cc45f8c9',
    stock: 14,
    reserve: 0,
  },

  /*4*/ {
    id: '64b2516ba3999fa0cc45f8cb',
    stock: 15,
    reserve: 0,
  },

  /*5*/ {
    id: '64b2516ba3999fa0cc45f8cd',
    stock: 6,
    reserve: 0,
  },

  /*6*/ {
    id: '64b2516ba3999fa0cc45f8cf',
    stock: 8,
    reserve: 0,
  },

  /*7*/ {
    id: '64b2516ba3999fa0cc45f8d1',
    stock: 10,
    reserve: 0,
  },

  /*8*/ {
    id: '64b2516ba3999fa0cc45f8d3',
    stock: 11,
    reserve: 0,
  },

  /*9*/ {
    id: '64b2516ba3999fa0cc45f8d5',
    stock: 13,
    reserve: 0,
  },
];
