export const ArtSupplyStockArr = [
  /*0*/ {
    id: '64b2674ba1868fb92c1e68c9',
    stock: 24,
    reserve: 0,
  },

  /*1*/ {
    id: '64b2674ba1868fb92c1e68cb',
    stock: 12,
    reserve: 0,
  },

  /*2*/ {
    id: '64b2674ba1868fb92c1e68cd',
    stock: 15,
    reserve: 0,
  },

  /*3*/ {
    id: '64b2674ba1868fb92c1e68cf',
    stock: 10,
    reserve: 0,
  },

  /*4*/ {
    id: '64b2674ba1868fb92c1e68d1',
    stock: 13,
    reserve: 0,
  },

  /*5*/ {
    id: '64b2674ba1868fb92c1e68d3',
    stock: 9,
    reserve: 0,
  },

  /*6*/ {
    id: '64b2674ba1868fb92c1e68d5',
    stock: 8,
    reserve: 0,
  },

  /*7*/ {
    id: '64b2674ba1868fb92c1e68d7',
    stock: 12,
    reserve: 0,
  },

  /*8*/ {
    id: '64b2674ba1868fb92c1e68d9',
    stock: 9,
    reserve: 0,
  },

  /*9*/ {
    id: '64b2674ba1868fb92c1e68db',
    stock: 15,
    reserve: 0,
  },

  /*10*/ {
    id: '64b2674ba1868fb92c1e68dd',
    stock: 10,
    reserve: 0,
  },

  /*11*/ {
    id: '64b2674ba1868fb92c1e68df',
    stock: 8,
    reserve: 0,
  },

  /*12*/ {
    id: '64b2674ba1868fb92c1e68e1',
    stock: 8,
    reserve: 0,
  },

  /*13*/ {
    id: '64b2674ba1868fb92c1e68e3',
    stock: 8,
    reserve: 0,
  },
];
