export const PartySupplyStockArr = [
  /*0*/ {
    id: '64b285aefdfb2e8d44209b91',
    stock: 24,
    reserve: 0,
  },

  /*1*/ {
    id: '64b285aefdfb2e8d44209b93',
    stock: 14,
    reserve: 0,
  },

  /*2*/ {
    id: '64b285aefdfb2e8d44209b95',
    stock: 12,
    reserve: 0,
  },

  /*3*/ {
    id: '64b285aefdfb2e8d44209b97',
    stock: 10,
    reserve: 0,
  },

  /*4*/ {
    id: '64b285aefdfb2e8d44209b99',
    stock: 8,
    reserve: 0,
  },

  /*5*/ {
    id: '64b285aefdfb2e8d44209b9b',
    stock: 7,
    reserve: 0,
  },

  /*6*/ {
    id: '64b285aefdfb2e8d44209b9d',
    stock: 11,
    reserve: 0,
  },

  /*7*/ {
    id: '64b285aefdfb2e8d44209b9f',
    stock: 6,
    reserve: 0,
  },
];
