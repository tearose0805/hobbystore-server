import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ICart, ICartItem, ICartItemUID } from '../../interfaces/cart';
import { CartService } from '../../services/cart/cart.service';

@Controller('cart')
export class CartController {
  constructor(private cartService: CartService) {}

  @Get()
  getCart(): Promise<ICart[]> {
    return this.cartService.getCart();
  }
  @Get(':ID')
  getCartByUID(@Param() param): Promise<ICart> {
    return this.cartService.getCartByUID(param.ID);
  }

  @Get(':ID/:itemID') //не нужен для приложения
  getCartItem(@Param() param): Promise<ICart> {
    return this.cartService.getCartItem(param.ID, param.itemID);
  }

  @Post()
  addCartContent(@Body() body: ICartItemUID): Promise<ICart> {
    return this.cartService.addCartContent(body);
  }

  @Put(':ID')
  cartLoad(@Param('ID') ID: string, @Body() data: ICartItem): Promise<ICart> {
    return this.cartService.cartLoad(ID, data);
  }

  @Put(':ID/:itemID')
  increaseCartItemQuantity(@Param() param, @Body() data): Promise<ICart> {
    return this.cartService.increaseCartItemQuantity(
      param.ID,
      param.itemID,
      data,
    );
  }
}
