import { Body, Controller, Get, Post } from '@nestjs/common';
import { OrderService } from '../../services/order/order.service';
import { IOrder } from '../../interfaces/order';

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Get()
  getOrders(): Promise<IOrder[]> {
    return this.orderService.getOrders();
  }

  @Post()
  createOrder(@Body() body: IOrder): Promise<IOrder> {
    return this.orderService.createOrder(body);
  }
}
