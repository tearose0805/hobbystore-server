import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { PartysupplyStockService } from '../../../../services/partysupply/partysupply-stock/partysupply-stock.service';
import { IProductStock } from '../../../../interfaces/product-stock';

@Controller('partysupply/stock')
export class PartysupplyStockController {
  constructor(private partysupplyStockService: PartysupplyStockService) {}

  @Get()
  getPartySupplyStock(): Promise<IProductStock[]> {
    return this.partysupplyStockService.getPartySupplyStock();
  }

  @Get(':itemId')
  getPartySupplyStockByItemId(@Param() param): Promise<IProductStock> {
    return this.partysupplyStockService.getPartySupplyStockByItemId(
      param.itemId,
    );
  }

  @Post()
  addPartySupplyStock(): Promise<IProductStock[]> {
    return this.partysupplyStockService.addPartySupplyStock();
  }

  @Put()
  increaseReserve(@Body() body: IProductStock): Promise<IProductStock> {
    return this.partysupplyStockService.increaseReserve(body);
  }
}
