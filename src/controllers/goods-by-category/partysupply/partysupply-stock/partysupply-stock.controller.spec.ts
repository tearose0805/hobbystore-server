import { Test, TestingModule } from '@nestjs/testing';
import { PartysupplyStockController } from './partysupply-stock.controller';

describe('PartysupplyStockController', () => {
  let controller: PartysupplyStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PartysupplyStockController],
    }).compile();

    controller = module.get<PartysupplyStockController>(PartysupplyStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
