import { Test, TestingModule } from '@nestjs/testing';
import { PartysupplyController } from './partysupply.controller';

describe('PartysupplyController', () => {
  let controller: PartysupplyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PartysupplyController],
    }).compile();

    controller = module.get<PartysupplyController>(PartysupplyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
