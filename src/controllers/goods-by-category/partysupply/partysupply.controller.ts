import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { PartysupplyService } from '../../../services/partysupply/partysupply.service';
import { IProduct } from '../../../interfaces/product';
import { PaperProduct } from '../../../schemas/paper-products';
import { ProductRatingDTO } from '../../../DTO/product-rating-update-dto';

@Controller('occasion/partysupply')
export class PartysupplyController {
  constructor(private partysupplyService: PartysupplyService) {}

  @Get()
  getPartySupplyList(): Promise<IProduct[]> {
    return this.partysupplyService.getPartySupplyList();
  }

  @Get('/search/:data')
  searchPartySupply(@Param('data') data: string): Promise<IProduct[]> {
    return this.partysupplyService.searchPartySupply(data);
  }
  @Get(':id')
  getPartySupplyByID(@Param('id') id: string): Promise<IProduct> {
    return this.partysupplyService.getPartySupplyByID(id);
  }

  @Post()
  addPartySupply(): Promise<PaperProduct[]> {
    return this.partysupplyService.addPartySupply();
  }

  @Put(':id')
  updatePartySupply(
    @Param('id') id: string,
    @Body() data: ProductRatingDTO,
  ): Promise<IProduct> {
    return this.partysupplyService.updatePartySupply(id, data);
  }
}
