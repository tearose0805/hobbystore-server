import { Test, TestingModule } from '@nestjs/testing';
import { ArtsupplyController } from './artsupply.controller';

describe('ArtsupplyController', () => {
  let controller: ArtsupplyController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArtsupplyController],
    }).compile();

    controller = module.get<ArtsupplyController>(ArtsupplyController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
