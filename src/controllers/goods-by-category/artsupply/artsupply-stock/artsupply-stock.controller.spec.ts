import { Test, TestingModule } from '@nestjs/testing';
import { ArtsupplyStockController } from './artsupply-stock.controller';

describe('ArtsupplyStockController', () => {
  let controller: ArtsupplyStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArtsupplyStockController],
    }).compile();

    controller = module.get<ArtsupplyStockController>(ArtsupplyStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
