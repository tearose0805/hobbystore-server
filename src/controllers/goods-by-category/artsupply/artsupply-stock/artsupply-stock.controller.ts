import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ArtsupplyStockService } from '../../../../services/art-supply/artsupply-stock/artsupply-stock.service';
import { IProductStock } from '../../../../interfaces/product-stock';

@Controller('artsupply/stock')
export class ArtsupplyStockController {
  constructor(private artsupplyStockService: ArtsupplyStockService) {}
  @Get()
  getArtSupplyStock(): Promise<IProductStock[]> {
    return this.artsupplyStockService.getArtSupplyStock();
  }

  @Get(':itemId')
  getArtSupplyStockByItemId(@Param() param): Promise<IProductStock> {
    return this.artsupplyStockService.getArtSupplyStockByItemId(param.itemId);
  }

  @Post()
  addArtSupplyStock(): Promise<IProductStock[]> {
    return this.artsupplyStockService.addArtSupplyStock();
  }

  @Put()
  increaseReserve(@Body() body: IProductStock): Promise<IProductStock> {
    return this.artsupplyStockService.increaseReserve(body);
  }
}
