import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ArtSupplyService } from '../../../services/art-supply/art-supply.service';
import { IProduct } from '../../../interfaces/product';
import { PaperProduct } from '../../../schemas/paper-products';
import { ProductRatingDTO } from '../../../DTO/product-rating-update-dto';

@Controller('art/artsupply')
export class ArtsupplyController {
  constructor(private artSupplyService: ArtSupplyService) {}
  @Get()
  getArtSupplyList(): Promise<IProduct[]> {
    return this.artSupplyService.getArtSupplyList();
  }

  @Get('/search/:data')
  searchArtSupply(@Param('data') data: string): Promise<IProduct[]> {
    return this.artSupplyService.searchArtSupply(data);
  }
  @Get(':id')
  getArtSupplyByID(@Param('id') id: string): Promise<IProduct> {
    return this.artSupplyService.getArtSupplyByID(id);
  }

  @Post()
  addArtSupply(): Promise<PaperProduct[]> {
    return this.artSupplyService.addArtSupply();
  }

  @Put(':id')
  updateArtSupply(
    @Param('id') id: string,
    @Body() data: ProductRatingDTO,
  ): Promise<IProduct> {
    return this.artSupplyService.updateArtSupply(id, data);
  }
}
