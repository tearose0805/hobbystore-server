import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { IProduct } from '../../../interfaces/product';
import { PaperFlowerService } from '../../../services/paper-flower/paper-flower.service';
import { PaperProduct } from '../../../schemas/paper-products';
import { ProductRatingDTO } from '../../../DTO/product-rating-update-dto';

@Controller('papercraft/paper-flower')
export class PaperFlowerController {
  constructor(private paperFlowerService: PaperFlowerService) {}
  @Get()
  getPaperProductList(): Promise<IProduct[]> {
    return this.paperFlowerService.getPaperFlowerList();
  }

  @Get('/search/:data')
  searchPaperFlowers(@Param('data') data: string): Promise<IProduct[]> {
    return this.paperFlowerService.searchPaperFlowers(data);
  }
  @Get(':id')
  getPaperFlowerByID(@Param('id') id: string): Promise<IProduct> {
    return this.paperFlowerService.getPaperFlowerByID(id);
  }

  @Post()
  addPaperFlowers(): Promise<PaperProduct[]> {
    return this.paperFlowerService.addPaperFlowers();
  }

  @Put(':id')
  updatePaperFlowers(
    @Param('id') id: string,
    @Body() data: ProductRatingDTO,
  ): Promise<IProduct> {
    return this.paperFlowerService.updatePaperFlowers(id, data);
  }
}
