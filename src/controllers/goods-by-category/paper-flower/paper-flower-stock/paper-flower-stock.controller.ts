import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { PaperFlowersStockService } from '../../../../services/paper-flower/paper-flowers-stock/paper-flowers-stock.service';
import { IProductStock } from '../../../../interfaces/product-stock';

@Controller('paper-flower/stock')
export class PaperFlowerStockController {
  constructor(private paperFlowersStockService: PaperFlowersStockService) {}

  @Get()
  getPaperFlowerStock(): Promise<IProductStock[]> {
    return this.paperFlowersStockService.getPaperFlowerStock();
  }

  @Get(':itemId')
  getPaperFlowerStockByItemId(@Param() param): Promise<IProductStock> {
    return this.paperFlowersStockService.getPaperFlowerStockByItemId(
      param.itemId,
    );
  }

  @Post()
  addPaperFlowerStock(): Promise<IProductStock[]> {
    return this.paperFlowersStockService.addPaperFlowerStock();
  }

  @Put()
  increaseReserve(@Body() body: IProductStock): Promise<IProductStock> {
    return this.paperFlowersStockService.increaseReserve(body);
  }
}
