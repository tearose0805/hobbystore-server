import { Test, TestingModule } from '@nestjs/testing';
import { PaperFlowerStockController } from './paper-flower-stock.controller';

describe('PaperFlowerStockController', () => {
  let controller: PaperFlowerStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaperFlowerStockController],
    }).compile();

    controller = module.get<PaperFlowerStockController>(PaperFlowerStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
