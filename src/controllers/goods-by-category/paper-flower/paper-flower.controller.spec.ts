import { Test, TestingModule } from '@nestjs/testing';
import { PaperFlowerController } from './paper-flower.controller';

describe('PaperFlowerController', () => {
  let controller: PaperFlowerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaperFlowerController],
    }).compile();

    controller = module.get<PaperFlowerController>(PaperFlowerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
