import { Test, TestingModule } from '@nestjs/testing';
import { ColoringStockController } from './coloring-stock.controller';

describe('ColoringStockController', () => {
  let controller: ColoringStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ColoringStockController],
    }).compile();

    controller = module.get<ColoringStockController>(ColoringStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
