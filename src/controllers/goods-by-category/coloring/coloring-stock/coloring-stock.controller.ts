import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ColoringStockService } from '../../../../services/coloring/coloring-stock/coloring-stock.service';
import { IProductStock } from '../../../../interfaces/product-stock';

@Controller('coloring/stock')
export class ColoringStockController {
  constructor(private coloringStockService: ColoringStockService) {}
  @Get()
  getColoringStock(): Promise<IProductStock[]> {
    return this.coloringStockService.getColoringStock();
  }

  @Get(':itemId')
  getColoringStockByItemId(@Param() param): Promise<IProductStock> {
    return this.coloringStockService.getColoringStockByItemId(param.itemId);
  }

  @Post()
  addColoringStock(): Promise<IProductStock[]> {
    return this.coloringStockService.addColoringStock();
  }

  @Put()
  increaseReserve(@Body() body: IProductStock): Promise<IProductStock> {
    return this.coloringStockService.increaseReserve(body);
  }
}
