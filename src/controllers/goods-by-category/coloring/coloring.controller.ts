import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { ColoringService } from '../../../services/coloring/coloring.service';
import { IProduct } from '../../../interfaces/product';
import { PaperProduct } from '../../../schemas/paper-products';
import { ProductRatingDTO } from '../../../DTO/product-rating-update-dto';

@Controller('art/coloring')
export class ColoringController {
  constructor(private coloringService: ColoringService) {}
  @Get()
  getColoringList(): Promise<IProduct[]> {
    return this.coloringService.getColoringList();
  }

  @Get('/search/:data')
  searchColoring(@Param('data') data: string): Promise<IProduct[]> {
    return this.coloringService.searchColoring(data);
  }
  @Get(':id')
  getColoringByID(@Param('id') id: string): Promise<IProduct> {
    return this.coloringService.getColoringByID(id);
  }

  @Post()
  addColoring(): Promise<PaperProduct[]> {
    return this.coloringService.addColoring();
  }

  @Put(':id')
  updateColoring(
    @Param('id') id: string,
    @Body() data: ProductRatingDTO,
  ): Promise<IProduct> {
    return this.coloringService.updateColoring(id, data);
  }
}
