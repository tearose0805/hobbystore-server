import { Test, TestingModule } from '@nestjs/testing';
import { ColoringController } from './coloring.controller';

describe('ColoringController', () => {
  let controller: ColoringController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ColoringController],
    }).compile();

    controller = module.get<ColoringController>(ColoringController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
