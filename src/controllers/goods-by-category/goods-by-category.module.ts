import { Module } from '@nestjs/common';
import { PaperProductsController } from './paper-products/paper-products.controller';
import { PaperProduct, PaperProductSchema } from '../../schemas/paper-products';
import { MongooseModule } from '@nestjs/mongoose';
import { PaperProductsService } from '../../services/paper-products/paper-products.service';
import { PaperProductsStockController } from './paper-products/paper-products-stock/paper-products-stock.controller';
import {
  PaperProductStock,
  PaperProductStockSchema,
} from '../../schemas/paper-product-stock';
import { PaperProductsStockService } from '../../services/paper-products/paper-products-stock/paper-products-stock.service';
import { PaperFlower, PaperFlowerSchema } from '../../schemas/paper-flowers';
import { PaperFlowerController } from './paper-flower/paper-flower.controller';
import { PaperFlowerService } from '../../services/paper-flower/paper-flower.service';
import {
  PaperFlowerStock,
  PaperFlowerStockSchema,
} from '../../schemas/paper-flower-stock';
import { PaperFlowerStockController } from './paper-flower/paper-flower-stock/paper-flower-stock.controller';
import { PaperFlowersStockService } from '../../services/paper-flower/paper-flowers-stock/paper-flowers-stock.service';
import { ArtSupply, ArtSupplySchema } from '../../schemas/art-supply';
import { ArtsupplyController } from './artsupply/artsupply.controller';
import { ArtSupplyService } from '../../services/art-supply/art-supply.service';
import {
  ArtSupplyStock,
  ArtSupplyStockSchema,
} from '../../schemas/art-supply-stock';
import { ArtsupplyStockController } from './artsupply/artsupply-stock/artsupply-stock.controller';
import { ArtsupplyStockService } from '../../services/art-supply/artsupply-stock/artsupply-stock.service';
import { Coloring, ColoringSchema } from '../../schemas/coloring';
import { ColoringController } from './coloring/coloring.controller';
import { ColoringService } from '../../services/coloring/coloring.service';
import {
  ColoringStock,
  ColoringStockSchema,
} from '../../schemas/coloring-stock';
import { ColoringStockController } from './coloring/coloring-stock/coloring-stock.controller';
import { ColoringStockService } from '../../services/coloring/coloring-stock/coloring-stock.service';
import { PartySupply, PartySupplySchema } from '../../schemas/party-supply';
import {
  PartySupplyStock,
  PartySupplyStockSchema,
} from '../../schemas/party-supply-stock';
import { PartysupplyController } from './partysupply/partysupply.controller';
import { PartysupplyStockController } from './partysupply/partysupply-stock/partysupply-stock.controller';
import { PartysupplyStockService } from '../../services/partysupply/partysupply-stock/partysupply-stock.service';
import { PartysupplyService } from '../../services/partysupply/partysupply.service';

@Module({
  controllers: [
    PaperProductsController,
    PaperProductsStockController,
    PaperFlowerController,
    PaperFlowerStockController,
    ArtsupplyController,
    ArtsupplyStockController,
    ColoringController,
    ColoringStockController,
    PartysupplyController,
    PartysupplyStockController,
  ],
  imports: [
    MongooseModule.forFeature([
      { name: PaperProduct.name, schema: PaperProductSchema },
      { name: PaperProductStock.name, schema: PaperProductStockSchema },
      { name: PaperFlower.name, schema: PaperFlowerSchema },
      { name: PaperFlowerStock.name, schema: PaperFlowerStockSchema },
      { name: ArtSupply.name, schema: ArtSupplySchema },
      { name: ArtSupplyStock.name, schema: ArtSupplyStockSchema },
      { name: Coloring.name, schema: ColoringSchema },
      { name: ColoringStock.name, schema: ColoringStockSchema },
      { name: PartySupply.name, schema: PartySupplySchema },
      { name: PartySupplyStock.name, schema: PartySupplyStockSchema },
    ]),
  ],
  providers: [
    PaperProductsService,
    PaperProductsStockService,
    PaperFlowerService,
    PaperFlowersStockService,
    ArtSupplyService,
    ArtsupplyStockService,
    ColoringService,
    ColoringStockService,
    PartysupplyStockService,
    PartysupplyService,
  ],
})
export class GoodsByCategoryModule {}
