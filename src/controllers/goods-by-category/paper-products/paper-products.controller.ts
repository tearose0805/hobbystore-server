import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { IProduct } from '../../../interfaces/product';
import { PaperProductsService } from '../../../services/paper-products/paper-products.service';
import { PaperProduct } from '../../../schemas/paper-products';
import { ProductRatingDTO } from '../../../DTO/product-rating-update-dto';

@Controller('papercraft/paper')
export class PaperProductsController {
  constructor(private paperProductsService: PaperProductsService) {}

  @Get()
  getPaperProductList(): Promise<IProduct[]> {
    return this.paperProductsService.getPaperProductList();
  }

  @Get('/search/:data')
  searchPaperProducts(@Param('data') data: string): Promise<IProduct[]> {
    return this.paperProductsService.searchPaperProducts(data);
  }
  @Get(':id')
  getPaperProductByID(@Param('id') id: string): Promise<IProduct> {
    return this.paperProductsService.getPaperProductByID(id);
  }

  @Post()
  addPaperProducts(): Promise<PaperProduct[]> {
    return this.paperProductsService.addPaperProducts();
  }

  @Put(':id')
  updatePaperProduct(
    @Param('id') id: string,
    @Body() data: ProductRatingDTO,
  ): Promise<IProduct> {
    return this.paperProductsService.updatePaperProduct(id, data);
  }
}
