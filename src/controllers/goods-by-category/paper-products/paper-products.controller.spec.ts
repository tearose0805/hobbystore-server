import { Test, TestingModule } from '@nestjs/testing';
import { PaperProductsController } from './paper-products.controller';

describe('PaperProductsController', () => {
  let controller: PaperProductsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaperProductsController],
    }).compile();

    controller = module.get<PaperProductsController>(PaperProductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
