import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { PaperProductsStockService } from '../../../../services/paper-products/paper-products-stock/paper-products-stock.service';
import { IProductStock } from '../../../../interfaces/product-stock';

@Controller('paper/stock')
export class PaperProductsStockController {
  constructor(private paperProductsStockService: PaperProductsStockService) {}

  @Get()
  getPaperProductsStock(): Promise<IProductStock[]> {
    return this.paperProductsStockService.getPaperProductsStock();
  }

  @Get(':itemId')
  getPaperProductStockByItemId(@Param() param): Promise<IProductStock> {
    return this.paperProductsStockService.getPaperProductStockByItemId(
      param.itemId,
    );
  }

  @Post()
  addPaperProductsStock(): Promise<IProductStock[]> {
    return this.paperProductsStockService.addPaperProductsStock();
  }

  @Put()
  increaseReserve(@Body() body: IProductStock): Promise<IProductStock> {
    return this.paperProductsStockService.increaseReserve(body);
  }
}
