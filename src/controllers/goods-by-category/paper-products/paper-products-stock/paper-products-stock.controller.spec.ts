import { Test, TestingModule } from '@nestjs/testing';
import { PaperProductsStockController } from './paper-products-stock.controller';

describe('PaperProductsStockController', () => {
  let controller: PaperProductsStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PaperProductsStockController],
    }).compile();

    controller = module.get<PaperProductsStockController>(PaperProductsStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
