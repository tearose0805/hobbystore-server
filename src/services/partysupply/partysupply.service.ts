import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PartySupply } from '../../schemas/party-supply';
import { PartSupplyArr } from '../../assets/rawPartySupply';
import { ProductDTO } from '../../DTO/product-dto';

@Injectable()
export class PartysupplyService {
  goodsList = PartSupplyArr;
  goodsCount = this.goodsList.length;

  constructor(
    @InjectModel(PartySupply.name)
    private partySupplyModel: Model<PartySupply>,
  ) {}

  async getPartySupplyList(): Promise<PartySupply[]> {
    return this.partySupplyModel.find();
  }

  async getPartySupplyByID(id): Promise<PartySupply> {
    return this.partySupplyModel.findById(id);
  }
  async searchPartySupply(data): Promise<PartySupply[]> {
    return this.partySupplyModel.find(
      {
        $or: [
          { title: { $regex: data, $options: 'i' } },
          { type: { $regex: data, $options: 'i' } },
          { color: { $regex: data, $options: 'i' } },
          { product_number: { $regex: data } },
        ],
      },
      {},
      { limit: 5 },
    );
  }

  async addPartySupply(): Promise<PartySupply[]> {
    for (let i = 0; i < this.goodsCount; i++) {
      const item = new ProductDTO(
        this.goodsList[i].title,
        this.goodsList[i].type,
        this.goodsList[i].price,
        this.goodsList[i].description,
        this.goodsList[i].category,
        this.goodsList[i].subcategory,
        this.goodsList[i].img,
        this.goodsList[i].img_thumb,
        this.goodsList[i].size,
        this.goodsList[i].weight,
        this.goodsList[i].pack,
        this.goodsList[i].color,
        this.goodsList[i].quantity,
      );
      const goodsCard = new this.partySupplyModel(item);
      await goodsCard.save();
    }
    return this.getPartySupplyList();
  }

  async updatePartySupply(id, data): Promise<PartySupply> {
    console.log(id);
    return this.partySupplyModel.findByIdAndUpdate(
      id,
      {
        $set: {
          rating1: data.rating1,
          rating2: data.rating2,
          rating3: data.rating3,
          rating4: data.rating4,
          rating5: data.rating5,
        },
      },
      { returnDocument: 'after' },
    );
  }
}
