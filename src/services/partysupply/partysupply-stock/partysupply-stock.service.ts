import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PartySupplyStock } from '../../../schemas/party-supply-stock';
import { PartySupplyStockArr } from '../../../assets/rawPartySupplyStockArr';
import { ProductStockDTO } from '../../../DTO/product-stock-dto';

@Injectable()
export class PartysupplyStockService {
  goodsStock = PartySupplyStockArr;
  goodsStockCount = this.goodsStock.length;

  constructor(
    @InjectModel(PartySupplyStock.name)
    private partySupplyStockModel: Model<PartySupplyStock>,
  ) {}

  async getPartySupplyStock(): Promise<PartySupplyStock[]> {
    return this.partySupplyStockModel.find();
  }

  async addPartySupplyStock(): Promise<PartySupplyStock[]> {
    for (let i = 0; i < this.goodsStockCount; i++) {
      const item = new ProductStockDTO(
        this.goodsStock[i].id,
        this.goodsStock[i].stock,
        this.goodsStock[i].reserve,
      );
      const stockItem = new this.partySupplyStockModel(item);
      await stockItem.save();
    }
    return this.getPartySupplyStock();
  }
  async increaseReserve(body: PartySupplyStock): Promise<PartySupplyStock> {
    return this.partySupplyStockModel.findOneAndUpdate(
      { itemId: body.itemId },
      { $inc: { reserve: body.reserve } },
      { returnDocument: 'after' },
    );
  }

  async getPartySupplyStockByItemId(id: string): Promise<PartySupplyStock> {
    return this.partySupplyStockModel.findOne({ itemId: id });
  }
}
