import { Test, TestingModule } from '@nestjs/testing';
import { PartysupplyStockService } from './partysupply-stock.service';

describe('PartysupplyStockService', () => {
  let service: PartysupplyStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PartysupplyStockService],
    }).compile();

    service = module.get<PartysupplyStockService>(PartysupplyStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
