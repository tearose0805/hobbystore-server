import { Test, TestingModule } from '@nestjs/testing';
import { PartysupplyService } from './partysupply.service';

describe('PartysupplyService', () => {
  let service: PartysupplyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PartysupplyService],
    }).compile();

    service = module.get<PartysupplyService>(PartysupplyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
