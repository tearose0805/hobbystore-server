import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ArtSupply } from '../../schemas/art-supply';
import { Model } from 'mongoose';
import { ProductDTO } from '../../DTO/product-dto';
import { ArtSupplyArr } from '../../assets/rawArtSupplyArr';

@Injectable()
export class ArtSupplyService {
  goodsList = ArtSupplyArr;
  goodsCount = this.goodsList.length;

  constructor(
    @InjectModel(ArtSupply.name)
    private artSupplyModel: Model<ArtSupply>,
  ) {}

  async getArtSupplyList(): Promise<ArtSupply[]> {
    return this.artSupplyModel.find();
  }

  async getArtSupplyByID(id): Promise<ArtSupply> {
    return this.artSupplyModel.findById(id);
  }
  async searchArtSupply(data): Promise<ArtSupply[]> {
    return this.artSupplyModel.find(
      {
        $or: [
          { title: { $regex: data, $options: 'i' } },
          { type: { $regex: data, $options: 'i' } },
          { color: { $regex: data, $options: 'i' } },
          { product_number: { $regex: data } },
        ],
      },
      {},
      { limit: 3 },
    );
  }

  async addArtSupply(): Promise<ArtSupply[]> {
    for (let i = 0; i < this.goodsCount; i++) {
      const item = new ProductDTO(
        this.goodsList[i].title,
        this.goodsList[i].type,
        this.goodsList[i].price,
        this.goodsList[i].description,
        this.goodsList[i].category,
        this.goodsList[i].subcategory,
        this.goodsList[i].img,
        this.goodsList[i].img_thumb,
        this.goodsList[i].size,
        this.goodsList[i].weight,
        this.goodsList[i].pack,
        this.goodsList[i].color,
        this.goodsList[i].quantity,
      );
      const goodsCard = new this.artSupplyModel(item);
      await goodsCard.save();
    }
    return this.getArtSupplyList();
  }

  async updateArtSupply(id, data): Promise<ArtSupply> {
    console.log(id);
    return this.artSupplyModel.findByIdAndUpdate(
      id,
      {
        $set: {
          rating1: data.rating1,
          rating2: data.rating2,
          rating3: data.rating3,
          rating4: data.rating4,
          rating5: data.rating5,
        },
      },
      { returnDocument: 'after' },
    );
  }
}
