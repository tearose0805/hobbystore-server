import { Test, TestingModule } from '@nestjs/testing';
import { ArtSupplyService } from './art-supply.service';

describe('ArtSupplyService', () => {
  let service: ArtSupplyService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArtSupplyService],
    }).compile();

    service = module.get<ArtSupplyService>(ArtSupplyService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
