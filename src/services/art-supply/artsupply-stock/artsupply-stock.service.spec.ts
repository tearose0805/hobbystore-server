import { Test, TestingModule } from '@nestjs/testing';
import { ArtsupplyStockService } from './artsupply-stock.service';

describe('ArtsupplyStockService', () => {
  let service: ArtsupplyStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ArtsupplyStockService],
    }).compile();

    service = module.get<ArtsupplyStockService>(ArtsupplyStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
