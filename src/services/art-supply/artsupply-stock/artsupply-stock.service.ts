import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ArtSupplyStock } from '../../../schemas/art-supply-stock';
import { ArtSupplyStockArr } from '../../../assets/rawArtSupplyStockArr';
import { ProductStockDTO } from '../../../DTO/product-stock-dto';
import { Model } from 'mongoose';

@Injectable()
export class ArtsupplyStockService {
  goodsStock = ArtSupplyStockArr;
  goodsStockCount = this.goodsStock.length;

  constructor(
    @InjectModel(ArtSupplyStock.name)
    private artSupplyStockModel: Model<ArtSupplyStock>,
  ) {}
  async getArtSupplyStock(): Promise<ArtSupplyStock[]> {
    return this.artSupplyStockModel.find();
  }

  async addArtSupplyStock(): Promise<ArtSupplyStock[]> {
    for (let i = 0; i < this.goodsStockCount; i++) {
      const item = new ProductStockDTO(
        this.goodsStock[i].id,
        this.goodsStock[i].stock,
        this.goodsStock[i].reserve,
      );
      const stockItem = new this.artSupplyStockModel(item);
      await stockItem.save();
    }
    return this.getArtSupplyStock();
  }

  async increaseReserve(body: ArtSupplyStock): Promise<ArtSupplyStock> {
    return this.artSupplyStockModel.findOneAndUpdate(
      { itemId: body.itemId },
      { $inc: { reserve: body.reserve } },
      { returnDocument: 'after' },
    );
  }

  async getArtSupplyStockByItemId(id: string): Promise<ArtSupplyStock> {
    return this.artSupplyStockModel.findOne({ itemId: id });
  }
}
