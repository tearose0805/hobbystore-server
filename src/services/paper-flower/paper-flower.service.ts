import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaperFlower } from '../../schemas/paper-flowers';
import { Model } from 'mongoose';
import { ProductDTO } from '../../DTO/product-dto';
import { FlowersProductsArr } from '../../assets/rawFlowersProductArr';

@Injectable()
export class PaperFlowerService {
  goodsList = FlowersProductsArr;
  goodsCount = this.goodsList.length;

  constructor(
    @InjectModel(PaperFlower.name)
    private paperFlowerModel: Model<PaperFlower>,
  ) {}

  async getPaperFlowerList(): Promise<PaperFlower[]> {
    return this.paperFlowerModel.find();
  }

  async getPaperFlowerByID(id): Promise<PaperFlower> {
    return this.paperFlowerModel.findById(id);
  }
  async searchPaperFlowers(data): Promise<PaperFlower[]> {
    return this.paperFlowerModel.find(
      {
        $or: [
          { title: { $regex: data, $options: 'i' } },
          { type: { $regex: data, $options: 'i' } },
          { color: { $regex: data, $options: 'i' } },
          { product_number: { $regex: data } },
        ],
      },
      {},
      { limit: 3 },
    );
  }

  async addPaperFlowers(): Promise<PaperFlower[]> {
    for (let i = 0; i < this.goodsCount; i++) {
      const item = new ProductDTO(
        this.goodsList[i].title,
        this.goodsList[i].type,
        this.goodsList[i].price,
        this.goodsList[i].description,
        this.goodsList[i].category,
        this.goodsList[i].subcategory,
        this.goodsList[i].img,
        this.goodsList[i].img_thumb,
        this.goodsList[i].size,
        this.goodsList[i].weight,
        this.goodsList[i].pack,
        this.goodsList[i].color,
        this.goodsList[i].quantity,
      );
      const goodsCard = new this.paperFlowerModel(item);
      await goodsCard.save();
    }
    return this.getPaperFlowerList();
  }

  async updatePaperFlowers(id, data): Promise<PaperFlower> {
    console.log(id);
    return this.paperFlowerModel.findByIdAndUpdate(
      id,
      {
        $set: {
          rating1: data.rating1,
          rating2: data.rating2,
          rating3: data.rating3,
          rating4: data.rating4,
          rating5: data.rating5,
        },
      },
      { returnDocument: 'after' },
    );
  }
}
