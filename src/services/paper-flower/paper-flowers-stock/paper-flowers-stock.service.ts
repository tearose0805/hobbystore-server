import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaperFlowerStock } from '../../../schemas/paper-flower-stock';
import { Model } from 'mongoose';
import { ProductStockDTO } from '../../../DTO/product-stock-dto';
import { PaperFlowerStockArr } from '../../../assets/rawFlowersProductStockArr';

@Injectable()
export class PaperFlowersStockService {
  goodsStock = PaperFlowerStockArr;
  goodsStockCount = this.goodsStock.length;
  constructor(
    @InjectModel(PaperFlowerStock.name)
    private paperFlowerStockModel: Model<PaperFlowerStock>,
  ) {}

  async getPaperFlowerStock(): Promise<PaperFlowerStock[]> {
    return this.paperFlowerStockModel.find();
  }

  async addPaperFlowerStock(): Promise<PaperFlowerStock[]> {
    for (let i = 0; i < this.goodsStockCount; i++) {
      const item = new ProductStockDTO(
        this.goodsStock[i].id,
        this.goodsStock[i].stock,
        this.goodsStock[i].reserve,
      );
      const stockItem = new this.paperFlowerStockModel(item);
      await stockItem.save();
    }
    return this.getPaperFlowerStock();
  }

  async increaseReserve(body: PaperFlowerStock): Promise<PaperFlowerStock> {
    return this.paperFlowerStockModel.findOneAndUpdate(
      { itemId: body.itemId },
      { $inc: { reserve: body.reserve } },
      { returnDocument: 'after' },
    );
  }

  async getPaperFlowerStockByItemId(id: string): Promise<PaperFlowerStock> {
    return this.paperFlowerStockModel.findOne({ itemId: id });
  }
}
