import { Test, TestingModule } from '@nestjs/testing';
import { PaperFlowersStockService } from './paper-flowers-stock.service';

describe('PaperFlowersStockService', () => {
  let service: PaperFlowersStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PaperFlowersStockService],
    }).compile();

    service = module.get<PaperFlowersStockService>(PaperFlowersStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
