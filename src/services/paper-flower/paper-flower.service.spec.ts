import { Test, TestingModule } from '@nestjs/testing';
import { PaperFlowerService } from './paper-flower.service';

describe('PaperFlowerService', () => {
  let service: PaperFlowerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PaperFlowerService],
    }).compile();

    service = module.get<PaperFlowerService>(PaperFlowerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
