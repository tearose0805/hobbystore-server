import { Test, TestingModule } from '@nestjs/testing';
import { ColoringService } from './coloring.service';

describe('ColoringService', () => {
  let service: ColoringService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ColoringService],
    }).compile();

    service = module.get<ColoringService>(ColoringService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
