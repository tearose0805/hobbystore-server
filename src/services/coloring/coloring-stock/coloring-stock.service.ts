import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ColoringStock } from '../../../schemas/coloring-stock';
import { ColoringStockArr } from '../../../assets/rawColoringStockArr';
import { ProductStockDTO } from '../../../DTO/product-stock-dto';

@Injectable()
export class ColoringStockService {
  goodsStock = ColoringStockArr;
  goodsStockCount = this.goodsStock.length;

  constructor(
    @InjectModel(ColoringStock.name)
    private coloringStockModel: Model<ColoringStock>,
  ) {}

  async getColoringStock(): Promise<ColoringStock[]> {
    return this.coloringStockModel.find();
  }

  async addColoringStock(): Promise<ColoringStock[]> {
    for (let i = 0; i < this.goodsStockCount; i++) {
      const item = new ProductStockDTO(
        this.goodsStock[i].id,
        this.goodsStock[i].stock,
        this.goodsStock[i].reserve,
      );
      const stockItem = new this.coloringStockModel(item);
      await stockItem.save();
    }
    return this.getColoringStock();
  }

  async increaseReserve(body: ColoringStock): Promise<ColoringStock> {
    return this.coloringStockModel.findOneAndUpdate(
      { itemId: body.itemId },
      { $inc: { reserve: body.reserve } },
      { returnDocument: 'after' },
    );
  }

  async getColoringStockByItemId(id: string): Promise<ColoringStock> {
    return this.coloringStockModel.findOne({ itemId: id });
  }
}
