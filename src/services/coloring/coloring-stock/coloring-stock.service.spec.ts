import { Test, TestingModule } from '@nestjs/testing';
import { ColoringStockService } from './coloring-stock.service';

describe('ColoringStockService', () => {
  let service: ColoringStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ColoringStockService],
    }).compile();

    service = module.get<ColoringStockService>(ColoringStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
