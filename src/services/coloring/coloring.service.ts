import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Coloring } from '../../schemas/coloring';
import { ColoringArr } from '../../assets/rawColoringArr';
import { ProductDTO } from '../../DTO/product-dto';

@Injectable()
export class ColoringService {
  goodsList = ColoringArr;
  goodsCount = this.goodsList.length;

  constructor(
    @InjectModel(Coloring.name)
    private coloringModel: Model<Coloring>,
  ) {}

  async getColoringList(): Promise<Coloring[]> {
    return this.coloringModel.find();
  }

  async getColoringByID(id): Promise<Coloring> {
    return this.coloringModel.findById(id);
  }
  async searchColoring(data): Promise<Coloring[]> {
    return this.coloringModel.find(
      {
        $or: [
          { title: { $regex: data, $options: 'i' } },
          { type: { $regex: data, $options: 'i' } },
          { color: { $regex: data, $options: 'i' } },
          { product_number: { $regex: data } },
        ],
      },
      {},
      { limit: 3 },
    );
  }

  async addColoring(): Promise<Coloring[]> {
    for (let i = 0; i < this.goodsCount; i++) {
      const item = new ProductDTO(
        this.goodsList[i].title,
        this.goodsList[i].type,
        this.goodsList[i].price,
        this.goodsList[i].description,
        this.goodsList[i].category,
        this.goodsList[i].subcategory,
        this.goodsList[i].img,
        this.goodsList[i].img_thumb,
        this.goodsList[i].size,
        this.goodsList[i].weight,
        this.goodsList[i].pack,
        this.goodsList[i].color,
        this.goodsList[i].quantity,
      );
      const goodsCard = new this.coloringModel(item);
      await goodsCard.save();
    }
    return this.getColoringList();
  }

  async updateColoring(id, data): Promise<Coloring> {
    console.log(id);
    return this.coloringModel.findByIdAndUpdate(
      id,
      {
        $set: {
          rating1: data.rating1,
          rating2: data.rating2,
          rating3: data.rating3,
          rating4: data.rating4,
          rating5: data.rating5,
        },
      },
      { returnDocument: 'after' },
    );
  }
}
