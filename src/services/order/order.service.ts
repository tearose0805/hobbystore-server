import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Order } from '../../schemas/order';
import { OderDto } from '../../DTO/order-dto';
import { IOrder } from '../../interfaces/order';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order.name)
    private orderModel: Model<Order>,
  ) {}

  async getOrders(): Promise<Order[]> {
    return this.orderModel.find();
  }

  async createOrder(body: IOrder): Promise<Order> {
    const order = new OderDto(
      body.firstName,
      body.secondName,
      body.lastName,
      body.mobile,
      body.index,
      body.region,
      body.city,
      body.address,
      body.orderNumber,
      body.goodsItems,
      body.orderInfo,
    );
    const orderObj = new this.orderModel(order);
    return orderObj.save();
  }
}
