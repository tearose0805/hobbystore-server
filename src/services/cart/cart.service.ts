import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Cart } from '../../schemas/cart';
import { Model } from 'mongoose';
import { ICartItem, ICartItemUID } from '../../interfaces/cart';
import { CartDto } from '../../DTO/cart-dto';

@Injectable()
export class CartService {
  constructor(
    @InjectModel(Cart.name)
    private cartModel: Model<Cart>,
  ) {}

  async getCart(): Promise<Cart[]> {
    return this.cartModel.find();
  }

  async getCartByUID(ID: string): Promise<Cart> {
    console.log('Получили UID ', ID);
    return this.cartModel.findOne({ UID: ID });
  }

  async getCartItem(ID, cartItemID): Promise<Cart> {
    const cartDoc = await this.cartModel.findOne(
      { UID: ID },
      { cart: { $elemMatch: { itemID: cartItemID } } },
    );
    /* console.log(cartDoc.cart.length);
    if (cartDoc.cart.length) {
      console.log('Получили документ ', cartDoc.cart);
    } else {
      console.log('Товар отсутствует в корзине ', cartDoc.cart);
    }*/

    return cartDoc;
  }

  async addCartContent(body: ICartItemUID): Promise<Cart> {
    const cart = new CartDto(
      body.UID,
      body.itemID,
      body.quantity,
      body.category,
      body.subcategory,
    );
    const cartContent = new this.cartModel(cart);
    return cartContent.save();
  }

  async addItemToCart(ID, data): Promise<Cart> {
    return this.cartModel.findOneAndUpdate(
      ID,
      {
        $push: {
          cart: {
            itemID: data.itemID,
            quantity: data.quantity,
            category: data.category,
            subcategory: data.subcategory,
          },
        },
      },
      { returnDocument: 'after' },
    );
  }

  async increaseCartItemQuantity(ID, itemID, data): Promise<Cart> {
    console.log(ID);
    console.log(itemID);
    console.log(data);
    return this.cartModel.findOneAndUpdate(
      {
        UID: ID,
        'cart.itemID': itemID,
      },
      { $inc: { 'cart.$.quantity': data.quantity } },
      { returnDocument: 'after' },
    );
  }

  async cartLoad(ID, data): Promise<Cart> {
    const cartDoc = await this.getCartItem(ID, data.itemID);
    console.log('длина массива товаров ', cartDoc.cart.length);
    if (cartDoc.cart.length) {
      console.log('Товар есть в корзине');
      return this.increaseCartItemQuantity(ID, data.itemID, data);
    } else {
      console.log('Товара нет в корзине');
      return this.addItemToCart(ID, data);
    }
  }
}
