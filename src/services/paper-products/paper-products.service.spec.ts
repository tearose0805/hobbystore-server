import { Test, TestingModule } from '@nestjs/testing';
import { PaperProductsService } from './paper-products.service';

describe('PaperProductsService', () => {
  let service: PaperProductsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PaperProductsService],
    }).compile();

    service = module.get<PaperProductsService>(PaperProductsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
