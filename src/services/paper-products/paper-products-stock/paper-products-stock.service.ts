import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PaperProductStock } from '../../../schemas/paper-product-stock';
import { PaperProductStockArr } from '../../../assets/rawPaperProductStock';
import { ProductStockDTO } from '../../../DTO/product-stock-dto';

@Injectable()
export class PaperProductsStockService {
  goodsStock = PaperProductStockArr;
  goodsStockCount = this.goodsStock.length;

  constructor(
    @InjectModel(PaperProductStock.name)
    private paperProductStockModel: Model<PaperProductStock>,
  ) {}

  async getPaperProductsStock(): Promise<PaperProductStock[]> {
    return this.paperProductStockModel.find();
  }

  async addPaperProductsStock(): Promise<PaperProductStock[]> {
    for (let i = 0; i < this.goodsStockCount; i++) {
      const item = new ProductStockDTO(
        this.goodsStock[i].id,
        this.goodsStock[i].stock,
        this.goodsStock[i].reserve,
      );
      const stockItem = new this.paperProductStockModel(item);
      await stockItem.save();
    }
    return this.getPaperProductsStock();
  }

  async increaseReserve(body: PaperProductStock): Promise<PaperProductStock> {
    return this.paperProductStockModel.findOneAndUpdate(
      { itemId: body.itemId },
      { $inc: { reserve: body.reserve } },{returnDocument: 'after'}
    );
  }

  async getPaperProductStockByItemId(id: string): Promise<PaperProductStock> {
    return this.paperProductStockModel.findOne({ itemId: id });
  }
}
