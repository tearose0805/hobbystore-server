import { Test, TestingModule } from '@nestjs/testing';
import { PaperProductsStockService } from './paper-products-stock.service';

describe('PaperProductsStockService', () => {
  let service: PaperProductsStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PaperProductsStockService],
    }).compile();

    service = module.get<PaperProductsStockService>(PaperProductsStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
