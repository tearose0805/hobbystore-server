import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { PaperProduct } from '../../schemas/paper-products';
import { InjectModel } from '@nestjs/mongoose';
import { PaperProductsArr } from '../../assets/rawPaperProductArr';
import { ProductDTO } from '../../DTO/product-dto';

@Injectable()
export class PaperProductsService {
  goodsList = PaperProductsArr;
  goodsCount = this.goodsList.length;

  constructor(
    @InjectModel(PaperProduct.name)
    private paperProductModel: Model<PaperProduct>,
  ) {}

  async getPaperProductList(): Promise<PaperProduct[]> {
    return this.paperProductModel.find();
  }

  async getPaperProductByID(id): Promise<PaperProduct> {
    return this.paperProductModel.findById(id);
  }
  async searchPaperProducts(data): Promise<PaperProduct[]> {
    return this.paperProductModel.find(
      {
        $or: [
          { title: { $regex: data, $options: 'i' } },
          { type: { $regex: data, $options: 'i' } },
          { color: { $regex: data, $options: 'i' } },
          { product_number: { $regex: data } },
        ],
      },
      {},
      { limit: 3 },
    );
  }

  async addPaperProducts(): Promise<PaperProduct[]> {
    for (let i = 0; i < this.goodsCount; i++) {
      const item = new ProductDTO(
        this.goodsList[i].title,
        this.goodsList[i].type,
        this.goodsList[i].price,
        this.goodsList[i].description,
        this.goodsList[i].category,
        this.goodsList[i].subcategory,
        this.goodsList[i].img,
        this.goodsList[i].img_thumb,
        this.goodsList[i].size,
        this.goodsList[i].weight,
        this.goodsList[i].pack,
        this.goodsList[i].color,
        this.goodsList[i].quantity,
      );
      const goodsCard = new this.paperProductModel(item);
      await goodsCard.save();
    }
    return this.getPaperProductList();
  }

  async updatePaperProduct(id, data): Promise<PaperProduct> {
    console.log(id);
    return this.paperProductModel.findByIdAndUpdate(
      id,
      {
        $set: {
          rating1: data.rating1,
          rating2: data.rating2,
          rating3: data.rating3,
          rating4: data.rating4,
          rating5: data.rating5,
        },
      },
      { returnDocument: 'after' },
    );
  }
}
