import { IProductRating } from '../interfaces/product-rating';

export class ProductRatingDTO implements IProductRating {
  rating1: number;
  rating2: number;
  rating3: number;
  rating4: number;
  rating5: number;

}
