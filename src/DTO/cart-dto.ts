import { ICart } from '../interfaces/cart';

export class CartDto implements ICart {
  UID: string;
  cart: {
    itemID: string;
    quantity: number;
    category: string;
    subcategory: string;
  }[] = [];

  constructor(UID, itemID, quantity, category, subcategory) {
    this.UID = UID;
    const itemObj = {
      itemID: itemID,
      quantity: quantity,
      category: category,
      subcategory: subcategory,
    };
    this.cart.push(itemObj);
  }
}
