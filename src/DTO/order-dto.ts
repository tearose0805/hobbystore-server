import { IOderGoodsItem, IOderInfo, IOrder } from '../interfaces/order';

export class OderDto implements IOrder {
  firstName: string;
  secondName?: string;
  lastName: string;
  mobile: string;
  index?: string;
  region: string;
  city: string;
  address: string;
  orderNumber: number;
  goodsItems: IOderGoodsItem[] = [];
  orderInfo: IOderInfo[] = [];

  constructor(
    firstName,
    secondName,
    lastName,
    mobile,
    index,
    region,
    city,
    address,
    orderNumber,
    goodsItem,
    orderInfo,
  ) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.lastName = lastName;
    this.mobile = mobile;
    this.index = index;
    this.region = region;
    this.city = city;
    this.address = address;
    this.orderNumber = orderNumber;
    this.goodsItems = goodsItem;
    this.orderInfo = orderInfo;
  }
}
