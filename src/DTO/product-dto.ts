import { IProduct } from '../interfaces/product';

export class ProductDTO implements IProduct {
  title: string;
  type: string;
  price: number;
  description: string;
  category: string;
  subcategory: string;
  img: string;
  img_thumb;
  product_number: string;
  rating1: number;
  rating2: number;
  rating3: number;
  rating4: number;
  rating5: number;
  size?: string;
  weight?: string;
  pack?: string;
  color?: string;
  quantity?: string;

  maxNumber = 70000;
  minNumber = 60000;
  minCount = 1;
  maxCount = 10;
  constructor(
    title,
    type,
    price,
    description,
    category,
    subcategory,
    img,
    img_thumb,
    size,
    weight,
    pack,
    color,
    quantity,
  ) {
    this.title = title;
    this.type = type;
    this.price = price;
    this.description = description;
    this.category = category;
    this.subcategory = subcategory;
    this.img = img;
    this.img_thumb = img_thumb;
    this.size = size;
    this.weight = weight;
    this.pack = pack;
    this.color = color;
    this.quantity = quantity;
    this.product_number = this.generateProductNumber(
      this.minNumber,
      this.maxNumber,
    ).toString();
    this.rating1 = this.generateRatingNumber(this.minCount, this.maxCount);
    this.rating2 = this.generateRatingNumber(this.minCount, this.maxCount);
    this.rating3 = this.generateRatingNumber(this.minCount, this.maxCount);
    this.rating4 = this.generateRatingNumber(this.minCount, this.maxCount);
    this.rating5 = this.generateRatingNumber(this.minCount, this.maxCount);
  }

  generateProductNumber(minNumber, maxNumber): number {
    return Math.floor(Math.random() * (maxNumber - minNumber) + minNumber);
  }

  generateRatingNumber(minCount, maxCount): number {
    return Math.floor(Math.random() * (maxCount - minCount) + minCount);
  }
}
