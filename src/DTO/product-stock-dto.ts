import { IProductStock } from '../interfaces/product-stock';

export class ProductStockDTO implements IProductStock {
  itemId: string;
  stock: number;
  reserve: number;

  constructor(id, stock, reserve) {
    this.itemId = id;
    this.stock = stock;
    this.reserve = reserve;
  }
}
